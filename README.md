# Kubernetes
- Before you start, check that the following programs are installed:
1. docker
2. kubectl
3. terraform

## creating a new azure Kubernetes cluster with terraform
```
clone the "terraform_cluster" directory
```

## terraform commands to create:
- open cmd and change the path to the folder we created and run the following commands:
```
terraform init
terraform fmt
terraform validate
terraform plan
terraform apply
```

## associate the kubectl commands with the aks:
- open cmd and run the following commands:
```
az account set --subscription <your subscription id>
az aks get-credentials --resource-group <resource group name> --name <your cluster name>
```
## create the environments namespaces:
```
CLI Commands:
- to create the namespaces: 
    kubectl apply -f namespaces.yaml
- If you want to switch to a different namespace:
    kubectl config set-context --current --namespace=<namespace name>
- If you want to check the current namespace:
    kubectl config view --minify --output 'jsonpath={..namespace}'
- If you want to check all your namespaces:
    kubectl get namespaces
```
## terraform commands to delete:
- open cmd and change the path to the folder we created and run the following commands:
```
terraform destroy
```
